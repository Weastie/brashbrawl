from sys import argv
from sys import exit
from json import load

if len(argv) < 4:
	print("USAGE:")
	print("python convert.py <jsonfile> <name> <background>")
	exit()

with open(argv[1]) as f:
	c = load(f)

m = ""
w = c["width"]
for row in range(c["height"]):
	for col in range(w):
		tile = c["tilesets"][0]["tileproperties"][str(c["layers"][0]["data"][w*row + col]-1)]
		m += tile["tileNumber"]+"-"+tile["collidePlayer"]+"-"+tile["collideBullet"]+"-"
		if c["layers"][1]["data"][w*row+col]!=0:
			extra=c["tilesets"][1]["tileproperties"][str(c["layers"][1]["data"][w*row+col]-int(c["tilesets"][1]["firstgid"]))]
			m += extra["extraNumber"]+"-"+extra["collide"]
		else:
			m += "0-0"
		m += ","
	m = m[:-1] + "|"
print("name:"+argv[2])
print("bgcolor:"+argv[3])
print("map:"+m[:-1])
print("x")
