<?xml version="1.0" encoding="UTF-8"?>
<tileset name="Extras" tilewidth="64" tileheight="64" tilecount="25" columns="0">
 <tile id="0">
  <properties>
   <property name="collide" value="0"/>
   <property name="extraNumber" value="1"/>
  </properties>
  <image width="64" height="64" source="../assets/extras/1.png"/>
 </tile>
 <tile id="1">
  <properties>
   <property name="collide" value="0"/>
   <property name="extraNumber" value="2"/>
  </properties>
  <image width="64" height="64" source="../assets/extras/2.png"/>
 </tile>
 <tile id="2">
  <properties>
   <property name="collide" value="0"/>
   <property name="extraNumber" value="3"/>
  </properties>
  <image width="64" height="64" source="../assets/extras/3.png"/>
 </tile>
 <tile id="3">
  <properties>
   <property name="collide" value="0"/>
   <property name="extraNumber" value="4"/>
  </properties>
  <image width="64" height="64" source="../assets/extras/4.png"/>
 </tile>
 <tile id="4">
  <properties>
   <property name="collide" value="0"/>
   <property name="extraNumber" value="5"/>
  </properties>
  <image width="64" height="64" source="../assets/extras/5.png"/>
 </tile>
 <tile id="5">
  <properties>
   <property name="collide" value="0"/>
   <property name="extraNumber" value="6"/>
  </properties>
  <image width="64" height="64" source="../assets/extras/6.png"/>
 </tile>
 <tile id="6">
  <properties>
   <property name="collide" value="0"/>
   <property name="extraNumber" value="7"/>
  </properties>
  <image width="64" height="64" source="../assets/extras/7.png"/>
 </tile>
 <tile id="7">
  <properties>
   <property name="collide" value="0"/>
   <property name="extraNumber" value="8"/>
  </properties>
  <image width="64" height="64" source="../assets/extras/8.png"/>
 </tile>
 <tile id="8">
  <properties>
   <property name="collide" value="0"/>
   <property name="extraNumber" value="9"/>
  </properties>
  <image width="64" height="64" source="../assets/extras/9.png"/>
 </tile>
 <tile id="9">
  <properties>
   <property name="collide" value="1"/>
   <property name="extraNumber" value="10"/>
  </properties>
  <image width="64" height="64" source="../assets/extras/10.png"/>
 </tile>
 <tile id="10">
  <properties>
   <property name="collide" value="1"/>
   <property name="extraNumber" value="11"/>
  </properties>
  <image width="64" height="64" source="../assets/extras/11.png"/>
 </tile>
 <tile id="11">
  <properties>
   <property name="collide" value="1"/>
   <property name="extraNumber" value="12"/>
  </properties>
  <image width="64" height="64" source="../assets/extras/12.png"/>
 </tile>
 <tile id="12">
  <properties>
   <property name="collide" value="1"/>
   <property name="extraNumber" value="13"/>
  </properties>
  <image width="64" height="64" source="../assets/extras/13.png"/>
 </tile>
 <tile id="13">
  <properties>
   <property name="collide" value="1"/>
   <property name="extraNumber" value="14"/>
  </properties>
  <image width="64" height="64" source="../assets/extras/14.png"/>
 </tile>
 <tile id="14">
  <properties>
   <property name="collide" value="1"/>
   <property name="extraNumber" value="15"/>
  </properties>
  <image width="64" height="64" source="../assets/extras/15.png"/>
 </tile>
 <tile id="15">
  <properties>
   <property name="collide" value="1"/>
   <property name="extraNumber" value="16"/>
  </properties>
  <image width="64" height="64" source="../assets/extras/16.png"/>
 </tile>
 <tile id="16">
  <properties>
   <property name="collide" value="1"/>
   <property name="extraNumber" value="17"/>
  </properties>
  <image width="64" height="64" source="../assets/extras/17.png"/>
 </tile>
 <tile id="17">
  <properties>
   <property name="collide" value="0"/>
   <property name="extraNumber" value="18"/>
  </properties>
  <image width="64" height="64" source="../assets/extras/18.png"/>
 </tile>
 <tile id="18">
  <properties>
   <property name="collide" value="0"/>
   <property name="extraNumber" value="19"/>
  </properties>
  <image width="64" height="64" source="../assets/extras/19.png"/>
 </tile>
 <tile id="19">
  <properties>
   <property name="collide" value="1"/>
   <property name="tileNumber" value="20"/>
  </properties>
  <image width="64" height="64" source="../assets/extras/20.png"/>
 </tile>
 <tile id="20">
  <properties>
   <property name="collide" value="1"/>
   <property name="tileNumber" value="21"/>
  </properties>
  <image width="64" height="64" source="../assets/extras/21.png"/>
 </tile>
 <tile id="21">
  <properties>
   <property name="collide" value="1"/>
   <property name="tileNumber" value="22"/>
  </properties>
  <image width="64" height="64" source="../assets/extras/22.png"/>
 </tile>
 <tile id="22">
  <properties>
   <property name="collide" value="1"/>
   <property name="tileNumber" value="23"/>
  </properties>
  <image width="64" height="64" source="../assets/extras/23.png"/>
 </tile>
 <tile id="23">
  <properties>
   <property name="collide" value="1"/>
   <property name="tileNumber" value="24"/>
  </properties>
  <image width="64" height="64" source="../assets/extras/24.png"/>
 </tile>
 <tile id="24">
  <properties>
   <property name="collide" value="1"/>
   <property name="tileNumber" value="25"/>
  </properties>
  <image width="64" height="64" source="../assets/extras/25.png"/>
 </tile>
</tileset>
