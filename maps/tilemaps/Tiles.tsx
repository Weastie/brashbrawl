<?xml version="1.0" encoding="UTF-8"?>
<tileset name="Tiles" tilewidth="64" tileheight="64" tilecount="13" columns="0">
 <tile id="0">
  <properties>
   <property name="collideBullet" value="0"/>
   <property name="collidePlayer" value="0"/>
   <property name="tileNumber" value="1"/>
  </properties>
  <image width="64" height="64" source="../assets/tiles/1.png"/>
 </tile>
 <tile id="1">
  <properties>
   <property name="collideBullet" value="1"/>
   <property name="collidePlayer" value="1"/>
   <property name="tileNumber" value="2"/>
  </properties>
  <image width="64" height="64" source="../assets/tiles/2.png"/>
 </tile>
 <tile id="2">
  <properties>
   <property name="collideBullet" value="0"/>
   <property name="collidePlayer" value="0"/>
   <property name="tileNumber" value="3"/>
  </properties>
  <image width="64" height="64" source="../assets/tiles/3.png"/>
 </tile>
 <tile id="3">
  <properties>
   <property name="collideBullet" value="0"/>
   <property name="collidePlayer" value="1"/>
   <property name="tileNumber" value="11"/>
  </properties>
  <image width="64" height="64" source="../assets/tiles/11.png"/>
 </tile>
 <tile id="4">
  <properties>
   <property name="collideBullet" value="0"/>
   <property name="collidePlayer" value="0"/>
   <property name="tileNumber" value="4"/>
  </properties>
  <image width="64" height="64" source="../assets/tiles/4.png"/>
 </tile>
 <tile id="5">
  <properties>
   <property name="collideBullet" value="0"/>
   <property name="collidePlayer" value="0"/>
   <property name="tileNumber" value="5"/>
  </properties>
  <image width="64" height="64" source="../assets/tiles/5.png"/>
 </tile>
 <tile id="6">
  <properties>
   <property name="collideBullet" value="0"/>
   <property name="collidePlayer" value="0"/>
   <property name="tileNumber" value="6"/>
  </properties>
  <image width="64" height="64" source="../assets/tiles/6.png"/>
 </tile>
 <tile id="7">
  <properties>
   <property name="collideBullet" value="0"/>
   <property name="collidePlayer" value="0"/>
   <property name="tileNumber" value="7"/>
  </properties>
  <image width="64" height="64" source="../assets/tiles/7.png"/>
 </tile>
 <tile id="8">
  <properties>
   <property name="collideBullet" value="0"/>
   <property name="collidePlayer" value="0"/>
   <property name="tileNumber" value="8"/>
  </properties>
  <image width="64" height="64" source="../assets/tiles/8.png"/>
 </tile>
 <tile id="9">
  <properties>
   <property name="collideBullet" value="0"/>
   <property name="collidePlayer" value="0"/>
   <property name="tileNumber" value="9"/>
  </properties>
  <image width="64" height="64" source="../assets/tiles/9.png"/>
 </tile>
 <tile id="10">
  <properties>
   <property name="collideBullet" value="0"/>
   <property name="collidePlayer" value="1"/>
   <property name="tileNumber" value="5"/>
  </properties>
  <image width="64" height="64" source="../assets/tiles/5.png"/>
 </tile>
 <tile id="13">
  <properties>
   <property name="collideBullet" value="0"/>
   <property name="collidePlayer" value="0"/>
   <property name="tileNumber" value="10"/>
  </properties>
  <image width="64" height="64" source="../assets/tiles/10.png"/>
 </tile>
 <tile id="14">
  <properties>
   <property name="collideBullet" value="0"/>
   <property name="collidePlayer" value="0"/>
   <property name="tileNumber" value="12"/>
  </properties>
  <image width="64" height="64" source="../assets/tiles/12.png"/>
 </tile>
</tileset>
